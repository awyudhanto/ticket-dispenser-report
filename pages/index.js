import Head from 'next/head'
import React from 'react'
import { Grid, Button, Typography, Hidden, Backdrop, TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import Navbar from '../components/Navbar'

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export default function Home() {
  const classes = useStyles()
  const [open, setOpen] = React.useState(false)
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')

  const handleClose = () => {
    setOpen(false)
  }
  const handleToggle = () => {
    setOpen(!open)
  }
  
  const users = [
    {
      name: 'the boring company',
      path: '/users/The_Boring_Company-Logo.wine.svg'
    },
    {
      name: 'netflix',
      path: '/users/Netflix-Logo.wine.svg'
    },
    {
      name: 'fitbit',
      path: '/users/Fitbit-Logo.wine.svg'
    },
    {
      name: 'google',
      path: '/users/Google-Logo.wine.svg'
    },
    {
      name: 'airbnb',
      path: '/users/Airbnb-Logo.wine.svg'
    },
    {
      name: 'uber',
      path: '/users/Uber-Logo.wine.svg'
    },
  ]

  return (
    <div className='root'>
      <Head>
        <title>Ticket Dispenser Report</title>
        <link rel="icon" href="/timkado-logo-50px.png" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      </Head>

      <Grid container justify='center' alignItems='flex-start' className='container' >
        <Navbar handleToggle={handleToggle} />
        <Grid item xs={12} className='space' />
        <Hidden mdUp>
          <Grid item container xs={10} justify='center' alignItems="center">
            <img src='/undraw_progressive_app_m9ms 1 (1).svg' alt='illustration' className='illustration' />
          </Grid>
        </Hidden>
        <Grid item container xs={8} md={4} justify='flex-start' alignItems='flex-start'>
          <Grid item container xs={12} justify='flex-start' alignItems='flex-start' >
            <Typography variant='h4' className='title'>Build awesome apps to help your market.</Typography>
          </Grid>
          <Grid item container xs={12} justify='flex-start' alignItems='flex-start' >
            <Typography variant='h6' className='subtitle'>Built on standard web technology, Daisi help you manage your market better.</Typography>
          </Grid>
          <Grid item container xs={5} justify='flex-start' alignItems='flex-start' className='left-button' >
            <Button variant="outlined" size='large' className='not-transparent-btn' fullWidth>Get Started</Button>
          </Grid>
          <Grid item container xs={5} justify='flex-start' alignItems='flex-start' className='right-button' >
            <Button variant="outlined" size='large' className='transparent-btn' fullWidth>Let's Talk</Button>
          </Grid>
          <Grid item container xs={12} justify='flex-start' alignItems='flex-start' >
            <Typography variant='subtitle2' className='subtitle'>Start free trial.  * No credit card required.</Typography>
          </Grid>
        </Grid>
        <Hidden smDown>
          <Grid item xs={1} />
          <Grid item container xs={5} justify='center' alignItems="center">
            <img src='/undraw_progressive_app_m9ms 1 (1).svg' alt='illustration' className='illustration' />
          </Grid>
        </Hidden>
        <Grid item xs={12} className='space' />
        <Grid item container xs={12} justify='center' alignItems="center" className='user-container'>
          <Grid item xs={12} className='white-space' />
          <Grid item container xs={10} spacing={3} justify='center' alignItems="center">
            {
              users.map((user, index) => {
                return (
                  <Grid item xs={4} ms={4} md={2} key={index}>
                    <img src={user.path} alt={user.name} className='user-logo' />
                  </Grid>
                )
              })
            }
          </Grid>
          <Grid item xs={12} className='white-space' />
        </Grid>
        <Grid item container xs={12} justify='center' alignItems="center" className='footer-container'>
          <Grid item container xs={10} justify='center' alignItems="center">
            <Typography variant='subtitle2' style={{ color: '#eaeaea', textAlign: 'center' }}>Copyright © 2020 PT. Timkado Sejahtera Indonesia. All rights reserved.</Typography>
          </Grid>
        </Grid>
        <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
          <h1>hello</h1>
        </Backdrop>
      </Grid>
    </div>
  )
}
