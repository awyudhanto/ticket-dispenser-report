import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { Grid, Typography } from '@material-ui/core'
import ChartMenu from '../components/ChartMenu'

const RegDateChart = dynamic(import('../components/chart/RegDate'), {
  ssr: false
})

const RegHourChart = dynamic(import('../components/chart/RegHour'), {
  ssr: false
})

const RegEmailChart = dynamic(import('../components/chart/EmailDomain'), {
  ssr: false
})

const MobileDeviceCountChart = dynamic(import('../components/chart/MobileDeviceCount'), {
  ssr: false
})

const CarBrandsChart = dynamic(import('../components/chart/CarBrands'), {
  ssr: false
})

const PaymentTypeChart = dynamic(import('../components/chart/PaymentType'), {
  ssr: false
})

const SeatsNumberChart = dynamic(import('../components/chart/NumberOfSeats'), {
  ssr: false
})

const BudgetChart = dynamic(import('../components/chart/Budget.js'), {
  ssr: false
})

const LocationChart = dynamic(import('../components/chart/Location.js'), {
  ssr: false
})

export default function Report () {
  const router = useRouter()
  const { ch } = router.query
  const [data, setData] = useState([])
  const [carData, setCarData] = useState([])
  const [show, setShow] = useState('default')

  async function dataFetcher (file) {
    let reportData = await fetch(file)
    reportData = await reportData.json()
    setData(reportData)
  }

  async function carDataFetcher (file) {
    let reportData = await fetch(file)
    reportData = await reportData.json()
    console.log(reportData)
    setCarData(reportData)
  }

  useEffect(() => {
    dataFetcher('https://ticket-dispenser-report.vercel.app/json/efair.json')
    carDataFetcher('https://ticket-dispenser-report.vercel.app/json/carSurvey.json')
  }, [])

  useEffect(() => {
    setShow(ch)
  }, [ch])
  
  return (
    <>
      <Head>
        <title>My page title</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <Grid container justify='center' alignItems='center' style={{ minHeight: '100vh' }}>
        <Grid item container xs={3}>
          <ChartMenu />
        </Grid>
        <Grid item container xs={9} justify='center'>
          <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
            <Typography variant='h3'>e-fair Report</Typography>
          </Grid>
          {
            show === 'count-pid-by-date' ? 
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Count of pId by Registration Date</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <RegDateChart data={data} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'count-pid-by-hour' ?
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Count of pId by Registration Hour</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <RegHourChart data={data} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'email-domains' ? 
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Count of Registered Email Domain</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <RegEmailChart data={data} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'mobile-devices' ?
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Count of Mobile Devices That Used to Visit e-fair</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <MobileDeviceCountChart data={data} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'car-brands' ?
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Count of Prefered Car Brands</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <CarBrandsChart data={carData} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'payment-type' ?
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Count of Prefered Payment Type</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <PaymentTypeChart data={carData} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'number-of-seats' ?
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Count of Prefered Number of Seats</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <SeatsNumberChart data={carData} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'budgets' ?
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Comparison of Budget and Age</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <BudgetChart data={carData} />
                </Grid>
              </Grid> : ''
          }
          {
            show === 'locations' ?
              <Grid item container xs={10} sm={10} justify='center' alignItems='center'>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <Typography variant='h5'>Mapping of Registered User's Locations</Typography>
                </Grid>
                <Grid item container xs={12} justify='center' alignItems='center'>
                  <LocationChart data={data} />
                </Grid>
              </Grid> : ''
          }
        </Grid>
      </Grid>
    </>
  )
}