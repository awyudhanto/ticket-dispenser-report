import React, { useState, useEffect } from 'react'
import { Grid, Button } from '@material-ui/core'
import SignatureCanvas from 'react-signature-canvas'

export default function Experiment () {
  const [trimmedDataURL, setTrimmedDataURL] = useState(null)
  let sigPad = {}

  // const trim = () => {
  //   setTrimmedDataURL(sigPad.getTrimmedCanvas().toDataURL('image/png'))
  // }

  function dataURLToBlob(dataURL) {
    // Code taken from https://github.com/ebidel/filer.js
    var parts = dataURL.split(';base64,');
    var contentType = parts[0].split(":")[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;
    var uInt8Array = new Uint8Array(rawLength);
  
    for (var i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }
  
    return new Blob([uInt8Array], { type: contentType });
  }

  function download(dataURL, filename) {
    if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
      window.open(dataURL);
    } else {
      var blob = dataURLToBlob(dataURL);
      var url = window.URL.createObjectURL(blob);
  
      var a = document.createElement("a");
      a.style = "display: none";
      a.href = url;
      a.download = filename;
  
      document.body.appendChild(a);
      a.click();
  
      window.URL.revokeObjectURL(url);
    }
  }

  const downloadImg = () => {
    let sigImg = sigPad.getTrimmedCanvas().toDataURL('image/png')
    download(sigImg, 'signature.png')
  }

  return (
    <>
      <Grid container justify='center' alignItems='center' style={{ minHeight: '100vh', backgroundColor: '#659af3' }}>
        <Grid item container justify='center' alignItems='center' style={{ height: '10vh' }} />
        <Grid item container justify='center' alignItems='center' >
          <SignatureCanvas
            penColor='black'
            backgroundColor='rgba(255,255,255,100)'
            canvasProps={{
              width: 500, 
              height: 200,
              className: 'sigCanvas'
            }}
            ref={(ref) => { sigPad = ref }}
          />
        </Grid>
        {/* <Grid item container justify='center' alignItems='center' >
          <Button onClick={trim} >See Trimmed Signature</Button>
        </Grid>
        <Grid item container justify='center' alignItems='center' >
          {
            trimmedDataURL ? 
              <img src={trimmedDataURL} /> :
              null
          }
        </Grid> */}
        <Grid item container justify='center' alignItems='center' >
          <Button variant="contained" onClick={downloadImg} >Save Signature Image</Button>
        </Grid>
        <Grid item container justify='center' alignItems='center' style={{ height: '10vh' }} />
      </Grid>
    </>
  )
}