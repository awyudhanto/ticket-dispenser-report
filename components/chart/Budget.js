import React, { useState, useEffect } from 'react'
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';

export default function Budget ({ data }) {
  const [filteredData, setFilteredData] = useState([])

  useEffect(() => {
    if (data) {

      let uniqueData = []
      let dataStr = ''
      for (let i = 0; i < data.length; i++) {
        if (data[i].pId !== dataStr && data[i].budget && data[i].age) {
          dataStr = data[i].pId
          let individualBudget = data[i].budget / 1000000
          uniqueData.push({
            age: data[i].age,
            budget: individualBudget
          })
        }
      }
      
      setFilteredData(uniqueData)
    }
  }, [data])

  return (
    <>
      <ScatterChart width={730} height={250}
        margin={{ top: 20, right: 20, bottom: 10, left: 10 }}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis type='number' dataKey="budget" name="budget" unit="mio" />
        <YAxis type='number' dataKey="age" name="age" unit="yo" />
        {/* <ZAxis dataKey="z" range={[64, 144]} name="score" unit="km" /> */}
        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
        <Scatter name="Comparison" data={filteredData} fill="#8884d8" />
      </ScatterChart>
    </>
  )
}