import React, { useState, useEffect } from 'react'
import { Tooltip, Legend, PieChart, Pie } from 'recharts'
import { Grid, Typography } from '@material-ui/core'
import randomColor from 'randomcolor'

export default function EmailDomain ({ data }) {
  const [filteredData, setFilteredData] = useState([])
  const [uniqueDomain, setUniqueDomain] = useState([])

  useEffect(() => {
    if (data) {

      let dataTemp = [{
        name: 'not registering email',
        count: 0
      }]
      
      for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < dataTemp.length; j++) {
          if (!data[i][`email_Arr[1]`]) {
            dataTemp[0].count += 1
            break;
          } else if (data[i][`email_Arr[1]`] == dataTemp[j].name) {
            dataTemp[j].count += 1
            break;
          } else if (j == dataTemp.length - 1) {
            dataTemp.push({
              name: data[i][`email_Arr[1]`],
              count: 1
            })
            break;
          }
        }
      }

      dataTemp.sort(function(a, b) {
        if (a.count > b.count) {
          return -1;
        }
        if (a.count < b.count) {
          return 1;
        }
        return 0;
      });

      let onlyOne = {
        name: 'others',
        count: 0
      }
      let uniqueTemp = []
      let i = 0;
      while (i < dataTemp.length) {
        if (dataTemp[i].count === 1) {
          onlyOne.count += 1
          uniqueTemp.push(dataTemp[i].name)
          dataTemp.splice(i, 1)
        } else {
          i++
        }
      }

      setUniqueDomain(uniqueTemp)
      dataTemp.push(onlyOne)

      let colorsTemp = randomColor({ hue: 'random', luminosity: 'random', count: dataTemp.length })
      for (let i = 0; i < dataTemp.length; i++) {
        dataTemp[i].fill = colorsTemp[i]
      }

      setFilteredData(dataTemp)
    }
  }, [data])

  return (
    <>
      <PieChart width={800} height={300}>
        <Tooltip />
        <Legend />
        <Pie data={filteredData} dataKey="count" nameKey="name" cx="50%" cy="50%" innerRadius={40} outerRadius={80} fill="#fff" label />
      </PieChart>
      <Grid container justify='center' alignItems='center'>
        <Grid item container xs={10} justify='flex-start' alignItems='center'>
          <Typography variant='subtitle2'>*others consists of domains that are used to register only by one person. They are:</Typography>
        </Grid>
        <Grid item container xs={10} justify='flex-start' alignItems='center'>
          {
            uniqueDomain.map((domain, index) => {
              return (
                <Grid item container xs={3} key={index} justify='flex-start' alignItems='center'>
                  <Typography variant='subtitle2'>- {domain}</Typography>
                </Grid>
              )
            })
          }
        </Grid>
      </Grid>
    </>
  )
}