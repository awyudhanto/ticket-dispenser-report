import React, { useState, useEffect } from 'react'
import { Tooltip, Legend, PieChart, Pie } from 'recharts'
import randomColor from 'randomcolor'

export default function PaymentType ({ data }) {
  const [filteredData, setFilteredData] = useState([])

  useEffect(() => {
    if (data) {

      let dataTemp = [{
        name: 'not chosen any',
        count: 0
      }]

      for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < dataTemp.length; j++) {
          if (!data[i][`paymentType`]) {
            dataTemp[0].count += 1
            break;
          } else if (data[i][`paymentType`] == dataTemp[j].name) {
            dataTemp[j].count += 1
            break;
          } else if (j == dataTemp.length - 1) {
            dataTemp.push({
              name: data[i][`paymentType`],
              count: 1
            })
            break;
          }
        }
      }

      let colorsTemp = randomColor({ hue: 'random', luminosity: 'random', count: dataTemp.length })
      for (let i = 0; i < dataTemp.length; i++) {
        dataTemp[i].fill = colorsTemp[i]
      }

      setFilteredData(dataTemp)
    }
  }, [data])

  return (
    <>
      <PieChart width={800} height={375}>
        <Tooltip />
        <Legend />
        <Pie data={filteredData} dataKey="count" nameKey="name" cx="50%" cy="50%" innerRadius={0} outerRadius={100} fill="#fff" label />
      </PieChart>
    </>
  )
}