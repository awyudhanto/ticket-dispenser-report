import React, { useState, useEffect } from 'react'
import { BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar } from 'recharts'
import Plot from 'react-plotly.js';

export default function CarBrand ({ data }) {
  const [filteredData, setFilteredData] = useState([])

  useEffect(() => {
    if (data) {

      let dataTemp = [{
        name: 'not chosen any',
        count: 0
      }]

      for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < dataTemp.length; j++) {
          if (!data[i][`carBrands_SplitResultList`]) {
            dataTemp[0].count += 1
            break;
          } else if (data[i][`carBrands_SplitResultList`] == dataTemp[j].name) {
            dataTemp[j].count += 1
            break;
          } else if (j == dataTemp.length - 1) {
            dataTemp.push({
              name: data[i][`carBrands_SplitResultList`],
              count: 1
            })
            break;
          }
        }
      }

      setFilteredData(dataTemp)
    }
  }, [data])

  return (
    <>
      {
        filteredData.length > 0 ? 
          <BarChart width={730} height={250} data={filteredData}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Bar dataKey="count" fill="#8884d8" />
          </BarChart> : <h1>loading</h1>
      }
    </>
  )
}