import React, { useState, useEffect } from 'react'
import { BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar } from 'recharts'
import Plot from 'react-plotly.js';

export default function RegDate ({ data }) {
  const [filteredData, setFilteredData] = useState([])
  const [allDate, setAllDate] = useState([])
  const [allCount, setAllCount] = useState([])

  useEffect(() => {
    if (data) {

      let resDate = ['19 Jan']
      let resCount = [0]

      const result = data.map(user => {
        const date = new Date(user['Reg_First(date)'])
        let month = ''
        if (date.getMonth() == 1) {
          month = 'Feb'
        } else {
          month = 'Jan'
        }
        user.regDate = `${date.getDate()} ${month}`
        return user
      })

      let temp = []
      for (let i = 18; i <= 31; i++) {
        let tempData = {
          name: `${i} Jan`,
          date: new Date(`2021-01-${i}`),
          count: 0
        }
        for (let j = 0; j < result.length; j++) {
          if (result[j].regDate == tempData.name) {
            tempData.count += 1
          }
        }
        temp.push(tempData)
      }

      for (let i = 1; i <= 4; i++) {
        let tempData = {
          name: `${i} Feb`,
          date: new Date(`2021-01-${i}`),
          count: 0
        }
        for (let j = 0; j < result.length; j++) {
          if (result[j].regDate == tempData.name) {
            tempData.count += 1
          }
        }
        temp.push(tempData)
      }
      setFilteredData(temp)

      for (let i = 0; i < result.length; i++) {
        for (let j = 0; j < resDate.length; j++) {
          if (resDate[j] == result[i].regDate) {
            resCount[j] += 1
          } else if (j == resDate.length - 1) {
            resDate.push(result[i].regDate)
            resCount.push(1)
          }
        }
      }

      setAllDate(resDate)
      setAllCount(resCount)

    }
  }, [data])

  return (
    <>
      {
        filteredData.length > 0 ? 
          <BarChart width={730} height={250} data={filteredData}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Bar dataKey="count" fill="#8884d8" />
          </BarChart> :
          <h1>loading</h1>
      }
      {/* {
        allDate.length == 0 ? 
          <h1>loading...</h1> :
          <Plot
            data={[{ x: allDate, y: allCount, type: 'bar', marker: {color: '#ab63fa'}, name: 'Bar' }]}
            layout={{ plotBackground: '#f3f6fa', margin: {t:0, r: 0, l: 20, b: 30} }}
            config={{displayModeBar: true}}
          />
      } */}
    </>
  )
}