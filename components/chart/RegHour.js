import React, { useState, useEffect } from 'react'
import { CartesianGrid, XAxis, YAxis, Tooltip, Legend, LineChart, Line } from 'recharts'

export default function RegHour ({ data }) {
  const [filteredData, setFilteredData] = useState([])

  useEffect(() => {
    if (data) {
      let timeData = []
      for (let i = 0; i < 24; i++) {
        let timeTemp = {
          name: `${i}`,
          count: 0
        }
        for (let j = 0; j < data.length; j++) {
          if (data[j]['Reg_First(hour)'].length != 0 && timeTemp.name == Number(data[j]['Reg_First(hour)'])) {
            timeTemp.count += 1
          }
        }
        timeData.push(timeTemp)
      }
      setFilteredData(timeData)
    }
  }, [data])

  return (
    <>
      {
        filteredData.length > 0 ?
          <LineChart width={730} height={250} data={filteredData} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Line dataKey="count" stroke="#8884d8" />
          </LineChart> : <h1>loading...</h1>
      }
    </>
  )
}