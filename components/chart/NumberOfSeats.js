import React, { useState, useEffect } from 'react'
import { BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar } from 'recharts'
import Plot from 'react-plotly.js';

export default function NumberOfSeats ({ data }) {
  const [filteredData, setFilteredData] = useState([])

  useEffect(() => {
    if (data) {

      let dataTemp = []

      for (let i = 1; i <= 8; i++) {
        let temp = {
          name: i,
          count: 0
        }
        for (let j = 0; j < data.length; j++) {
          if (data[j][`seatNumber_SplitResultList`] == i) {
            temp.count += 1
          } 
        }
        dataTemp.push(temp)
      }

      setFilteredData(dataTemp)
    }
  }, [data])

  return (
    <>
      {
        filteredData.length > 0 ? 
          <BarChart width={730} height={250} data={filteredData}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Bar dataKey="count" fill="#8884d8" />
          </BarChart> : <h1>loading</h1>
      }
    </>
  )
}