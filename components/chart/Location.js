import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { MapContainer, TileLayer, Marker, Popup, Circle, CircleMarker } from 'react-leaflet'
import { Grid, Typography } from '@material-ui/core'

export default function Location ({ data }) {
  const [filteredData, setFilteredData] = useState([])

  useEffect(() => {
    if (data) {
      let dataTemp = []

      for (let i = 0; i < data.length; i++) {
        if (dataTemp.length == 0) {
          dataTemp.push({
            city: data[i]['Reg_First(city)'],
            lat: data[i]['Reg_First(latitude)'],
            long: data[i]['Reg_First(longitude)'],
            count: 1
          })
        } else {
          for (let j = 0; j < dataTemp.length; j++) {
            if (data[i]['Reg_First(latitude)']) {
              if (dataTemp[j].lat == data[i]['Reg_First(latitude)'] && dataTemp[j].long == data[i]['Reg_First(longitude)']) {
                dataTemp[j].count += 1
                break;
              } else if (j == dataTemp.length - 1) {
                dataTemp.push({
                  city: data[i]['Reg_First(city)'],
                  lat: data[i]['Reg_First(latitude)'],
                  long: data[i]['Reg_First(longitude)'],
                  count: 1
                })
                break;
              }
            }
          }
        }
      }

      console.log(dataTemp)
      setFilteredData(dataTemp)
    }
  }, [data])

  return (
    <>
      <Head>
        <title>My page title</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossOrigin=''
        />
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
          integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
          crossOrigin=''
        />
      </Head>

      <Grid container justify='center' alignItems='center'>
        {
          filteredData.length !== 0 ?
            <MapContainer center={[-5.6145, 111.7122]} zoom={6} scrollWheelZoom={true} style={{ height: '70vh' }}>
              <TileLayer
                attribution={'&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              {
                filteredData.map((data, index) => {
                  return (
                    <Circle key={index} center={[data.lat, data.long]} radius={5000} pathOptions={{ color: 'red' }}>
                      <Popup>
                        <Typography>City: { data.city }</Typography>
                        <Typography>Latitude: { data.lat }</Typography>
                        <Typography>Longitude: { data.long }</Typography>
                        <Typography>Count: { data.count }</Typography>
                      </Popup>
                    </Circle>
                  )
                })
              }
            </MapContainer> : <h1>Loading...</h1>
        }
      </Grid>
    </>
  )
}