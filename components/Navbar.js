import React from 'react'
import { AppBar, Toolbar, Typography, Button, Grid, Hidden, IconButton, Menu, MenuItem } from '@material-ui/core'
import HomeIcon from '@material-ui/icons/Home';
import LockIcon from '@material-ui/icons/Lock';
import MenuIcon from '@material-ui/icons/Menu';

export default function Navbar ({handleToggle}) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <AppBar position='static'>
        <Toolbar className='navbar'>
          <Grid container >
            <Grid container item xs={9} sm={6} justify='center' alignItems='center'>
              <img src='/timkado-logo-50px.png' alt='timkado-logo' />
              <Typography className='navbar-title' variant='h4'>Daisi</Typography>
            </Grid>
            <Grid container item xs={3} sm={6} justify='flex-end' alignItems='center'>
              <Hidden xsDown>
                <Button
                  className='navbar-btn'
                  startIcon={<HomeIcon />}
                >
                  Home
                </Button>
                <Button
                  className='navbar-btn'
                  startIcon={<LockIcon />}
                  onClick={handleToggle}
                >
                  Login
                </Button>
              </Hidden>
              <Hidden smUp>
                <IconButton
                  aria-label="more"
                  aria-controls="long-menu"
                  aria-haspopup="true"
                  onClick={handleClick}
                >
                  <MenuIcon />
                </IconButton>
                <Menu
                  id="long-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={open}
                  onClose={handleClose}
                >
                  <MenuItem>
                    <Button
                      className='navbar-btn'
                      startIcon={<HomeIcon />}
                    >
                      Home
                    </Button>
                  </MenuItem>
                  <MenuItem>
                    <Button
                      className='navbar-btn'
                      startIcon={<LockIcon />}
                      onClick={handleToggle}
                    >
                      Login
                    </Button>
                  </MenuItem>
                </Menu>
              </Hidden>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </>
  )
}