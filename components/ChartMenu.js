import React, { useEffect, useState } from 'react';
import Link from 'next/link'
import { makeStyles } from '@material-ui/core/styles';
import { 
  CssBaseline, 
  Drawer, 
  AppBar, 
  Toolbar, 
  List, 
  Typography, 
  Grid,
  Divider, 
  ListItem, 
  ListItemIcon, 
  ListItemText 
} from '@material-ui/core'
import BarChartIcon from '@material-ui/icons/BarChart';
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import PieChartIcon from '@material-ui/icons/PieChart';
import ScatterPlotIcon from '@material-ui/icons/ScatterPlot';
import PublicIcon from '@material-ui/icons/Public';

const drawerWidth = 300;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
}));

export default function ChartMenu () {
  const classes = useStyles();

  return (
    <>
      <div className={classes.root}>
        <CssBaseline />
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
          anchor="left"
        >
          <Grid container justify='center' alignItems='center' className={classes.toolbar}>
            <img src='/timkado.jpg' style={{ maxHeight: '70%', maxWidth: '90%' }} />
          </Grid>
          <Divider />
          <List>
            <Link href='/report?ch=#'>
              <ListItem button>
                <ListItemText primary='Car Preferences' />
              </ListItem>
            </Link>
            <Link href='/report?ch=car-brands'>
              <ListItem button>
                <ListItemIcon><BarChartIcon /></ListItemIcon>
                <ListItemText primary='Car Brands' />
              </ListItem>
            </Link>
            <Link href='/report?ch=payment-type'>
              <ListItem button>
                <ListItemIcon><DonutLargeIcon /></ListItemIcon>
                <ListItemText primary='Payment Type' />
              </ListItem>
            </Link>
            <Link href='/report?ch=number-of-seats'>
              <ListItem button>
                <ListItemIcon><BarChartIcon /></ListItemIcon>
                <ListItemText primary='Number of Seats' />
              </ListItem>
            </Link>
            <Link href='/report?ch=budgets'>
              <ListItem button>
                <ListItemIcon><ScatterPlotIcon /></ListItemIcon>
                <ListItemText primary='Budgets' />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            <Link href='/report?ch=locations'>
              <ListItem button>
                <ListItemIcon><PublicIcon /></ListItemIcon>
                <ListItemText primary='Location' />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            <Link href='/report?ch=email-domains'>
              <ListItem button>
                <ListItemIcon><DonutLargeIcon /></ListItemIcon>
                <ListItemText primary='Count of Email Domains' />
              </ListItem>
            </Link>
            <Link href='/report?ch=mobile-devices'>
              <ListItem button>
                <ListItemIcon><PieChartIcon /></ListItemIcon>
                <ListItemText primary='Count of Mobile Devices' />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>  
            <Link href='/report?ch=count-pid-by-date'>
              <ListItem button>
                <ListItemIcon><BarChartIcon /></ListItemIcon>
                <ListItemText primary='Count of pId By Date' />
              </ListItem>
            </Link>
            <Link href='/report?ch=count-pid-by-hour'>
              <ListItem button>
                <ListItemIcon><ShowChartIcon /></ListItemIcon>
                <ListItemText primary='Count of pId By Hour' />
              </ListItem>
            </Link>
          </List>
          <Divider />
        </Drawer>
      </div>
    </>
  )
}